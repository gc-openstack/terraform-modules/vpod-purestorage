resource "null_resource" "purefilesystem" {
    provisioner "local-exec" {
        command = "ansible-playbook ${path.module}/ansible/purestorage.yml -e 'vpod_name=${var.vpod_name}' -e 'poddata_filesystem_name=${var.poddata_filesystem_name}' -e 'pure_api_token=${var.pure_api_token}' -e 'poddata_filesystem_size=${var.poddata_filesystem_size}' -e 'nethome_filesystem_name=${var.nethome_filesystem_name}' -e 'nethome_filesystem_size=${var.nethome_filesystem_size}' -e 'storage_net_mtu=${var.storage_net_mtu}' -e 'storage_vlan=${var.storage_vlan}' -e 'cidr_hint=${var.cidr_hint}' -e 'pure_api_endpoint=${var.pure_api_endpoint}' -C --diff"
    }
    provisioner "local-exec" {
        command = "ansible-playbook ${path.module}/ansible/purestorage.yml -e 'vpod_name=${var.vpod_name}' -e 'poddata_filesystem_name=${var.poddata_filesystem_name}' -e 'pure_api_token=${var.pure_api_token}' -e 'poddata_filesystem_size=${var.poddata_filesystem_size}' -e 'nethome_filesystem_name=${var.nethome_filesystem_name}' -e 'nethome_filesystem_size=${var.nethome_filesystem_size}' -e 'storage_net_mtu=${var.storage_net_mtu}' -e 'storage_vlan=${var.storage_vlan}' -e 'cidr_hint=${var.cidr_hint}' -e 'pure_api_endpoint=${var.pure_api_endpoint}'"
    }
    triggers = {
        filesystemnames = join(",", [ var.nethome_filesystem_name, var.poddata_filesystem_name ])
    }
}