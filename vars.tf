variable "poddata_filesystem_size" {
    type    = string
}

variable "poddata_filesystem_name" {
    type = string
}

variable "nethome_filesystem_size" {
    type    = string
}

variable "nethome_filesystem_name" {
    type = string
}
variable "pure_api_token" {
    type = string
}

variable "vpod_name" {
  type = string
}

variable "storage_net_mtu" {
  type = number
}

variable "storage_vlan" {
  type = number
}

variable "cidr_hint" {
  type = number
}

variable "pure_api_endpoint" {
  type = string
}