# vpod-purestorage

Terraform Module: vpod-purestorage
==================================

This Terraform module makes use of Ansible to communicate with the Pure Flashblade storage array to configure filesystems and network configuration. 

Prerequisites
=============
Packages
--------
* Ansible
* Pure Flashblade Ansible collection

Setup
-----
    python3 -m venv tools
    source tools/bin/activate
    pip install ansible
    ansible-galaxy collection install purestorage.flashblade

Variables
=========

* poddata_filesystem_size - the size of the filesystem to be provisioned (default: 20T)
* poddata_filesystem_name - the name of the filesystem to be provisioned
* nethome_filesystem_size - the size of the filesystem to be provisioned (default: 20T)
* nethome_filesystem_name - the name of the filesystem to be provisioned
* pure_api_token - an API token from the Pure Flashblade interface (should not be checked into config without being encrypted)
* vpod_name - the name of the vPOD being created
* storage_net_mtu - the MTU size set on the storage network
* storage_vlan - the VLAN used on the vPOD storage network
* cidr_hint - number to be used for the third octet of the storage subnet
* pure_api_endpoint - the hostname or IP address of the Pure Flashblade API endpoint

Usage
=====
This module should be used once per vPOD. 

    module "purestorage" {
        source                = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-purestorage.git"
        pure_api_token        = var.pure_api_token
        poddata_filesystem_name  = var.poddata_filesystem_name
        poddata_filesystem_size  = var.poddata_filesystem_size
        nethome_filesystem_name  = var.nethome_filesystem_name
        nethome_filesystem_size  = var.nethome_filesystem_size
        vpod_name             = var.vpod_name
        storage_net_mtu       = var.storage_net_mtu
        storage_vlan          = var.storage_vlan
        cidr_hint             = var.cidr_hint
    }

with example tfvars:

    vpod_name         = "vpod7"
    ctrl_vlan         = 1790
    rnic_vlan         = 2790
    storage_vlan      = 1207
    cidr_hint         = 7
    ipum_csv          = "rnic_pod24.csv"
    vpod_floatingip   = "38.83.162.187"
    poplaraz          = "usclt-pod1-lr24"
    assigned_ipums    = ["lr24-ipum1", "lr24-ipum2", "lr24-ipum3", "lr24-ipum4", "lr24-ipum5", "lr24-ipum6", "lr24-ipum7", "lr24-ipum8", "lr24-ipum9", "lr24-ipum10", "lr24-ipum11", "lr24-ipum12", "lr24-ipum13", "lr24-ipum14", "lr24-ipum15", "lr24-ipum16" ]
    flavor            = "r6525.full"
    poddata_filesystem_name = "customer1"
    nethome_filesystem_name = "customer1-nethome"